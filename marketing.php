<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Real Estate Marketing Services</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Marketing</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                 <li class="breadcrumb-item"><a>Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Marketing</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">                  
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="img/marketingimg.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">
                            <article class="pe-md-5 p-4 p-md-0">
                                <h3 class="h3 fbold uline">Real Estate Marketing</h3>
                                <p>Owning a piece of land is the common Indian's dream. At Building Blocks Group, we fulfil this dream by driving opportunities in the area of real estate. Working across Indian subcontinent, we are a company made of Indians, by the Indians, and working for the Indians.</p>      
                                <p>A group of seasoned professionals with a collective experience of over three decades lead Building Blocks Group with core values of Promise of Delivery, Transparency, Large-Scale Development, Integrity, Growth, and Excellence to deliver ‘The Land of Prosperity’ to our customers.</p>                         
                            </article>
                    </div>
                </div>                
            </div>                       
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>