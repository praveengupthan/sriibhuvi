<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Srii Bhuvi Developers Careers</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Careers</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Careers</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">
            <!-- container -->
            <div class="container">
                <h3 class="fsbold h2 fblue text-center">Let's build together</h3>
                 <!-- row -->
                <div class="row py-2 my-md-5">
                    <!-- col -->
                    <div class="col-md-3">
                        <div class="careerCol">
                            <div class="careerColicon">
                                <span class="icon-design icomoon"></span>
                            </div>
                            <h5 class="fsbold pt-3">Challenging projects</h5>
                            <p class="text-center">Our Project managers are tasked with keeping a site running smoothly, safely, within schedule and on budget. </p>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-3">
                        <div class="careerCol">
                            <div class="careerColicon">
                                <span class="icon-clients icomoon"></span>
                            </div>
                            <h5 class="fsbold pt-3">Awesome Clients</h5>
                            <p class="text-center">We have the privilege to work with amazing Clients and we develop with them impressive Construction Properties in many locations</p>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-3">
                        <div class="careerCol">
                            <div class="careerColicon">
                                <span class="icon-handshakes icomoon"></span>
                            </div>
                            <h5 class="fsbold pt-3">Award winning team</h5>
                            <p class="text-center">Our Team Award may be presented each year as part of the Annual Employee Recognition Program.</p>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-3">
                        <div class="careerCol">
                            <div class="careerColicon">
                                <span class="icon-challenges icomoon"></span>
                            </div>
                            <h5 class="fsbold pt-3">Challenging Environment</h5>
                            <p class="text-center">Our challenging task or job requires great effort and determination that we provide.</p>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-md-8">  
                        <h4 class="fsbold fblue pb-3">Current Job Openings</h4>
                        <!-- openings jobs row -->
                        <div class="jobOpenings">
                             <!-- accordion -->
                             <div class="accordion" id="accordionExample">
                                <div class="accordion-item border-bottom">
                                    <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Senior Accountant
                                    </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Experience:</strong></p>
                                                <p>5- 6 Years</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Type:</strong></p>
                                                <p>Accounts, Finance, Tax, Company Secretary, Audit</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>No. Of Positions:</strong></p>
                                                <p>10</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Location:</strong></p>
                                                <p>Hyderabad, Secunderabad</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Description:</strong></p>
                                                <ul class="listItems">
                                                    <li>Responsible for handling Accounts & Audit for the organisation</li>
                                                    <li>Extensive Expertise in Accounts & Audit</li>
                                                    <li>Good communication skills in Hindi, English & Telugu</li>
                                                    <li>Team work and management skills</li>
                                                </ul>
                                            </article>
                                            <p class="pb-0 mb-0">For More Details <a href="javascript:void(0)" class="fblue fsbold">Contact us</a></p>
                                            <p>Email us <a href="emailto:info@sriibhuvidevelopers.com" class="fblue fsbold">info@sriibhuvidevelopers.com</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Junior Marketing Executives
                                    </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Experience:</strong></p>
                                                <p>5- 6 Years</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Type:</strong></p>
                                                <p>Accounts, Finance, Tax, Company Secretary, Audit</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>No. Of Positions:</strong></p>
                                                <p>10</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Location:</strong></p>
                                                <p>Hyderabad, Secunderabad</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Description:</strong></p>
                                                <ul class="listItems">
                                                    <li>Responsible for handling Accounts & Audit for the organisation</li>
                                                    <li>Extensive Expertise in Accounts & Audit</li>
                                                    <li>Good communication skills in Hindi, English & Telugu</li>
                                                    <li>Team work and management skills</li>
                                                </ul>
                                            </article>
                                            <p class="pb-0 mb-0">For More Details <a href="javascript:void(0)" class="fblue fsbold">Contact us</a></p>
                                            <p>Email us <a href="emailto:info@sriibhuvidevelopers.com" class="fblue fsbold">info@sriibhuvidevelopers.com</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Senior Telecaller /  Tele Marketer
                                    </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Experience:</strong></p>
                                                <p>5- 6 Years</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Type:</strong></p>
                                                <p>Accounts, Finance, Tax, Company Secretary, Audit</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>No. Of Positions:</strong></p>
                                                <p>10</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Location:</strong></p>
                                                <p>Hyderabad, Secunderabad</p>
                                            </article>
                                            <article>
                                                <p class="pb-0 mb-0"><strong>Job Description:</strong></p>
                                                <ul class="listItems">
                                                    <li>Responsible for handling Accounts & Audit for the organisation</li>
                                                    <li>Extensive Expertise in Accounts & Audit</li>
                                                    <li>Good communication skills in Hindi, English & Telugu</li>
                                                    <li>Team work and management skills</li>
                                                </ul>
                                            </article>
                                            <p class="pb-0 mb-0">For More Details <a href="javascript:void(0)" class="fblue fsbold">Contact us</a></p>
                                            <p>Email us <a href="emailto:info@sriibhuvidevelopers.com" class="fblue fsbold">info@sriibhuvidevelopers.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!--/ accordion -->
                        </div>
                        <!--/ openings jobs row -->
                    </div>
                    <!--/ left col -->
                    <!-- right col -->
                    <div class="col-md-4">
                        <h4 class="fsbold pt-3 pt-md-0">Send your Updated Resume</h4>
                         <p>Please share your following details, Our Manager will contact you for further Employement Process.</p>
                         <p><strong><a href="mailto:career@sriibhuvidevelopers.com">career@sriibhuvidevelopers.com</a></strong></p>
                        <form class="pt-3 whitebox mb-3 d-none">
                           
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="Name" placeholder="Your Name">
                                <label for="Name">Your Name</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="Email" placeholder="Enter Email">
                                <label for="Email">Email address</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="Phone" placeholder="Phone Number">
                                <label for="Phone">Phone Number</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="city" placeholder="city">
                                <label for="city">City</label>
                            </div>
                             <div class="form-floating">
                                <input type="file" class="form-control" id="resume" placeholder="resume">
                            </div>
                            <small>Upload only word, pdf format</small>
                            <input type="button" value="Submit" class="brdlink w-100 d-block mt-3">
                        </form>
                        <img src="img/careerban.png" alt="" class="img-fluid w-100">
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>