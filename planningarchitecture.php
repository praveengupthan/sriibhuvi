<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Planning &amp; Architecture</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Planning &amp; Architecture</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                 <li class="breadcrumb-item"><a>Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Planning &amp; Architecture</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">          

            <!-- why choose us -->          
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="img/planningarchitecture.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">Planning</h3>
                                 <p>Srii Bhuvi provides services for planning. Company specialists work in a variety of planning projects such as regional, municipal and local plans. Srii Bhuvi has also experience with frame plans and neighbourhood planning. These planning projects included Strategic Environmental Assessments (SEA), BREEAM environmental assessment of communities, transport plans, traffic noise assessments and cartography.</p>     
                                 
                                 <div class="highletSec">
                                     <h6><i>Srii Bhuvi has extensive experience in regional, municipal and local planning projects at all stages.</i></h6>
                                 </div>
                             </article>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-6 order-md-last">
                            <img src="img/architecture.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">Structural Design</h3>
                                 <p>Srii Bhuvi has wide-ranging experience and expertise in structural design and offers turn-key consulting services ranging from preliminary stages to final design, as well as tender offers, construction management, supervision and maintenance.    </p>
                                
                                 <div class="highletSec">
                                     <h6><i>"We have four simple values that guide our behaviour and culture of innovation. Our values permeate through everything we do, including our relationships with our clients, our partners, our suppliers and the communities where we work..".</i></h6>
                                 </div>                               
                             </article>
                        </div>
                    </div>
                </div>           
            <!--/ why choose us -->
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>