<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projects</title>
     <?php include 'includes/styles.php'?>
     <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Projects</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Projects</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">
            <div class="container">
                <!-- project items -->
                <div class="projectsItems">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-completed-tab" data-bs-toggle="pill" data-bs-target="#pills-completed" type="button" role="tab" aria-controls="pills-completed" aria-selected="true">Completed</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-ongoing-tab" data-bs-toggle="pill" data-bs-target="#pills-ongoing" type="button" role="tab" aria-controls="pills-ongoing" aria-selected="false">Ongoing</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-upcoming-tab" data-bs-toggle="pill" data-bs-target="#pills-upcoming" type="button" role="tab" aria-controls="pills-upcoming" aria-selected="false">Upcoming</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-completed" role="tabpanel" aria-labelledby="pills-completed-tab">

                           <!-- project item -->
                           <?php
for ($i = 0; $i < count($completedProjectItem); $i++) {?>
                           <div class="projectItem">
                               <div class="row">
                                   <div class="col-md-6">
                                       <figure class="position-relative">
                                           <img src="img/projects/completed/<?php echo $completedProjectItem[$i][0] ?>" class="img-fluid w-100">
                                           <span class="badge bg-success position-absolute"><?php echo $completedProjectItem[$i][1] ?></span>
                                       </figure>
                                   </div>
                                   <div class="col-md-6">
                                       <h5 class="fsbold"><?php echo $completedProjectItem[$i][2] ?></h5>
                                       <p class="p-0 m-0"><small><?php echo $completedProjectItem[$i][3] ?></small></p>
                                       <p class="fgreen fsbold d-none"><?php echo $completedProjectItem[$i][4] ?></p>
                                       <p class="pb-3"><?php echo $completedProjectItem[$i][5] ?></p>
                                       <a href="<?php echo $completedProjectItem[$i][6] ?>" class="brdlink">Contact us</a>
                                   </div>
                               </div>
                           </div>
                           <?php }?>
                           <!--/ project item -->

                        </div>

                        <div class="tab-pane fade" id="pills-ongoing" role="tabpanel" aria-labelledby="pills-ongoing-tab">
                            <!-- project item -->
                           <?php
for ($i = 0; $i < count($ongointProjectItem); $i++) {?>
                           <div class="projectItem">
                               <div class="row">
                                   <div class="col-md-6">
                                       <figure class="position-relative">
                                           <img src="img/projects/current/<?php echo $ongointProjectItem[$i][0] ?>" class="img-fluid w-100">
                                           <span class="badge bg-success position-absolute"><?php echo $ongointProjectItem[$i][1] ?></span>
                                       </figure>
                                   </div>
                                   <div class="col-md-6">
                                       <h5 class="fsbold"><?php echo $ongointProjectItem[$i][2] ?></h5>
                                       <p class="p-0 m-0"><small><?php echo $ongointProjectItem[$i][3] ?></small></p>
                                       <p class="fgreen fsbold"><?php echo $ongointProjectItem[$i][4] ?></p>
                                       <p class="pb-3"><?php echo $ongointProjectItem[$i][5] ?></p>
                                      <div class="d-flex justify-content-between">
                                        <a href="<?php echo $ongointProjectItem[$i][6] ?>" class="brdlink">Contact us</a>
                                        <a class="align-self-center" href="<?php echo $ongointProjectItem[$i][7] ?>" target="_blank"> Directions</a>
                                      </div>
                                   </div>
                               </div>
                           </div>
                           <?php }?>
                           <!--/ project item -->
                        </div>

                        <div class="tab-pane fade" id="pills-upcoming" role="tabpanel" aria-labelledby="pills-upcoming-tab">
                           <!-- project item -->
                           <?php
for ($i = 0; $i < count($upcomingProjectItem); $i++) {?>
                           <div class="projectItem">
                               <div class="row">
                                   <div class="col-md-6">
                                       <figure class="position-relative">
                                           <img src="img/projects/upcoming/<?php echo $upcomingProjectItem[$i][0] ?>" class="img-fluid w-100">
                                           <span class="badge bg-success position-absolute"><?php echo $upcomingProjectItem[$i][1] ?></span>
                                       </figure>
                                   </div>
                                   <div class="col-md-6">
                                       <h5 class="fsbold"><?php echo $upcomingProjectItem[$i][2] ?></h5>
                                       <p class="p-0 m-0"><small><?php echo $upcomingProjectItem[$i][3] ?></small></p>
                                       <p class="fgreen fsbold"><?php echo $upcomingProjectItem[$i][4] ?></p>
                                       <p class="pb-3"><?php echo $upcomingProjectItem[$i][5] ?></p>
                                       <a href="<?php echo $upcomingProjectItem[$i][6] ?>" class="brdlink">Contact us</a>
                                       <a href="<?php echo $upcomingProjectItem[$i][7] ?>" target="_blank"> Directions</a>
                                   </div>
                               </div>
                           </div>
                           <?php }?>
                           <!--/ project item -->
                        </div>
                    </div>
                </div>
                <!--/ project items -->
            </div>
        </div>
        <!--/ sub page main -->

    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>