<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Interior Design</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Interior Design</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                 <li class="breadcrumb-item"><a>Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Interior Design</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">                  
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="img/interiordesign01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">
                            <article>
                                <h3 class="h3 fbold uline">Why Residential Interior Designers?</h3>
                                <p>Srii Bhuvi Developers, most renowned and leading interior designing company in Hyderabad, Telangana was established in 2019 by a group of interior designers, architects and interior decorator professionals. Our Srii Bhuvi Interior designers deal with all kinds of interior designs, home interior design, home decors, commercial designing.</p> 
                                <p>Our interior archietct's turnkey project execution method offers a complete interior design package, which helps you get the best interior designs and interior decorator services for your home at affordable price and within the comitted timelines for delivery.</p>                               
                            </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 order-md-last">
                        <img src="img/interiordesign02.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <article>
                            <h3 class="h3 fbold uline">Interior Designers & Decorators</h3>
                            <p>Our interior designers and architects use next-gen Design, Technology, skilled work force & Implementation methods, to solve not just today’s Home Interiors needs, but even the ones you don’t foresee for tomorrow.  With our specialized interior design and interior decor capabilities, we have been ranked the Best Interior Designers in Hyderabad since last 5 years. </p>  
                            <ul class="listItems">
                                <li>Commercial Interior Design</li>
                                <li>Residential Interior</li>
                                <li>Hospitality Interior Design</li>
                            </ul>                                                     
                        </article>
                    </div>
                </div>
            </div>                       
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>