var swiper = new Swiper('.homeSwiper', {
    spaceBetween: 30,
    speed: 500,
    effect: 'fade',
    fadeEffect: {
        crossFade: true
    },
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

});


//home services swiper 
var swiper = new Swiper('.homeServicesSwiper', {
    slidesPerView: 1,
    spaceBetween: 10,
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
    },
    // init: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
        1024: {
            slidesPerView: 4,
            spaceBetween: 10,
        },
    }
});


//click event to move top
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('#movetop').fadeIn();
    } else {
        $('#movetop').fadeOut();
    }
});

//click event to scroll top
$('#movetop').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 'fast')
});

//on scroll add class to header 
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.fixed-top').addClass('fixed-theme');
    } else {
        $('.fixed-top').removeClass('fixed-theme');
    }
});

baguetteBox.run('.grid-gallery', { animation: 'slideIn' });