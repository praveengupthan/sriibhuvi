<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Srii Bhuvi Developers</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main>
        <!-- home slider -->
        <div class="homeSlider">
            <!-- custom container -->
            <div class="customContainer">
                <div class="row">
                    <div class="col-md-4">
                        <div class="swiper-container homeSwiper">
                            <div class="swiper-wrapper aos-item" data-aos="fade-up">
                                <?php 
                                for($i=0; $i<count($homeSliderItem);$i++) {?>
                                <div class="swiper-slide">
                                    <h1 class="fbold h1"><?php echo $homeSliderItem[$i][0]?></h1>
                                    <p><?php echo $homeSliderItem[$i][1]?></p>
                                </div>                                
                                <?php } ?>                                
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
            <!--/ custom container -->
        </div>
        <!--/ home slider -->
        <!-- sliders for services -->
        <div class="homeServices">
            <div class="customContainer">
                <div class="d-sm-flex titleBox aos-item">
                    <h2 class="fbold pe-md-2 me-md-2 align-self-center"> Services </h2>
                    <p>Identifying the best construction materials and understanding their properties we ensure your
                        home's solid contents and structure.</p>
                </div>
            </div>
            <!-- full container for slider -->
            <div class="homeServicesList">
                <!-- Swiper -->
                <div class="swiper-container homeServicesSwiper">
                    <div class="swiper-wrapper">
                        <?php 
                        for($i=0; $i<count($homeServiceItem);$i++) {?>
                        <div class="swiper-slide">
                            <span class="<?php echo $homeServiceItem[$i][0]?> icomoon aos-item" data-aos="fade-up"></span>
                            <h3 class="fbold aos-item" data-aos="fade-up"><?php echo $homeServiceItem[$i][1]?></h3>
                            <p class="aos-item" data-aos="fade-up"><?php echo $homeServiceItem[$i][2]?></p>
                            <a class="aos-item" data-aos="fade-up" href="<?php echo $homeServiceItem[$i][3]?>">Read More....</a>
                        </div>
                        <?php } ?>                       
                    </div>
                    <!-- Add Pagination -->
                    <!-- Add Arrows -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>
            </div>
            <!--/ full container for slider -->
        </div>
        <!--/ sliders for services  -->
        <!-- materials section -->
        <div class="container-fluid g-0">
            <div class="row g-0">
                <div class="col-md-8">
                    <img src="img/materaialsimg.jpg" alt="" class="img-fluid w-100">
                </div>
                <div class="col-md-4 align-self-center">
                    <article class="px-md-5 p-3">
                        <h6 class="fsbold text-uppercase flightblue aos-item" data-aos="fade-up">Material Research</h6>
                        <h2 class="fbold aos-item" data-aos="fade-up">Fine Touch of Luxury</h2>
                        <p class="aos-item" data-aos="fade-up">Materials research is an important part of the design process in construction.  Engineering theory is largely based on the characteristics of construction materials, which makes it necessary to carry out tests on them before embarking on construction. Pre-testing the materials often makes it possible to see and prevent various costly and even dangerous problems. Comprehensive testing during the design and implementation process often leads to lower construction costs.</p>
                    </article>
                </div>
            </div>
        </div>
        <!--/ materials section -->
        <!-- about section -->
        <div class="aboutSection">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <h2 class="fbold afterUnderling position-relative aos-item" data-aos="fade-up">We are on expert on this field better buildings solutions since 2019.</h2>
                        <p class="aos-item" data-aos="fade-down">We are committed to building a sustainable future by fostering a collaborative spirit that creates exceptional experiences, balanced relationships, and enhances a community’s built environment.  </p>
                    </div>
                </div>
                <div class="row justify-content-center aos-item" data-aos="fade-up">
                    <div class="col-md-3 col-6 text-center aboutCol">
                        <h3 class="fbold">13</h3>
                        <h6>Apartment Houses</h6>
                    </div>
                    <div class="col-md-3 col-6 text-center aboutCol">
                        <h3 class="fbold">10</h3>
                        <h6>Luxe Apartments</h6>
                    </div>
                    <div class="col-md-3 col-6 text-center aboutCol">
                        <h3 class="fbold">16</h3>
                        <h6>Luxury Villas</h6>
                    </div>
                    <div class="col-md-3 col-6 text-center aboutCol">
                        <h3 class="fbold">65</h3>
                        <h6>Happy Customers</h6>
                    </div>
                </div>
                <p>
                    <a href="about.php" class="link aos-item" data-aos="fade-up">Read More About us</a>
                </p>
            </div>
        </div>
        <!--/ about section -->
        <!-- interior section -->
        <div class="container-fluid g-0">
            <div class="row g-0">
                <div class="col-md-8 order-md-last">
                    <img src="img/interiorsimg.jpg" alt="" class="img-fluid w-100">
                </div>
                <div class="col-md-4 align-self-center">
                    <article class="ps-2 px-md-5 p-3">
                        <h6 class="fsbold text-uppercase flightblue aos-item" data-aos="fade-up">Best Interior Designers in Hyderabad</h6>
                        <h2 class="fbold aos-item" data-aos="fade-up">Superior Style</h2>
                        <p class="aos-item" data-aos="fade-up">We at Srii Bhuvi Developers understand how important is to have a dream house built according to your taste, style and requirements.   We take immense care in every step of design and execution of your dream house or office to match up to your aesthetics and budget. Our state of the art design team will help you visualize your future home with proper designs, post our executive team will take care of putting their best to make your home as you like based on the design approved by you.</p>
                    </article>
                </div>
            </div>
        </div>
        <!--/ interior section -->
        <!-- projects -->
        <div class="projectsHome">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-12 titlecol align-self-center">
                        <article class="ps-2 px-md-5 pb-3 pb-lg-0">
                            <h6 class="fsbold text-uppercase flightblue aos-item" data-aos="fade-up">On Going Project</h6>
                            <h2 class="fbold aos-item" data-aos="fade-up">Prime Avenue</h2>    
                            <p class="pb-4 m-0">Location: <small>Chitkul, Patancheru</small></p>                              
                            <a href="projects.php" class="link aos-item" data-aos="fade-up">View All Projects</a>
                        </article>
                    </div>
                    <!-- col -->
                   <div class="col-lg-5 col-12">
                       <figure class="position-relative">
                            <img src="img/projects/current/homeCurrent.png" class="img-fluid w-100">                                    
                        </figure>
                   </div>
                    <!--/ col -->                   
                </div>
            </div>
        </div>
        <!--/ projects-->
        <!-- contact Section -->
        <div class="contactSection">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h2 class="fbold aos-item" data-aos="fade-up">Discover the whole world of new possibilities with Srii Bhuvi Developers.</h2>
                        <p class="aos-item">Our estate agents will help you to choose the perfect location for the home of your dream or
                            you can look through the gallery of new residential complexes by yourself </p>
                        <a href="contact.php" class="aos-item" data-aos="fade-up">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
        <!--/ contact section -->
        <!-- partners -->
        <div class="homePartners d-none">
            <div class="customContainer">
                <h2 class="text-center fbold aos-item" data-aos="fade-up">Our Partners</h2>
                <div class="row">
                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="img/partners/part01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="img/partners/part02.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="img/partners/part03.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="img/partners/part04.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="img/partners/part05.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="img/partners/part06.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <!--/ partners -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>