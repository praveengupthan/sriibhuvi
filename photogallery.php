<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Photo Gallery</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Photo Gallery</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item"><a>Gallery</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Photo Gallery</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">                  
            <div class="container">
                <section class="gallery-block grid-gallery">   
                    <!-- one project -->
                    <div class="projectGallery">                       
                        <div class="heading">
                            <h4>PRN Residency, Villa Plot Venture, Shadnagar</h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/1.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/1.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/2.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/2.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/3.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/3.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/4.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/4.jpg">
                                </a>
                            </div>  
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/5.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/5.jpg">
                                </a>
                            </div>  
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/6.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/6.jpg">
                                </a>
                            </div>  
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/7.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/7.jpg">
                                </a>
                            </div> 
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/8.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/8.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/9.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/9.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/10.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/10.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/11.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/11.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/12.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/12.jpg">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                <a class="lightbox" href="img/gallery/projects/prnresidency/13.jpg">
                                    <img class="img-fluid image scale-on-hover" src="img/gallery/projects/prnresidency/13.jpg">
                                </a>
                            </div>                                  
                        </div>
                    </div>
                    <!--/ one project -->                                      

                </section>       
            </div>                       
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>