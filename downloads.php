<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Downloads</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Downloads</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>                               
                                <li class="breadcrumb-item active" aria-current="page">Downloads</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">                  
            <div class="container">       
              <!-- table -->
              <div class="table-responsive-sm">
                    <table class="table">
                         <thead class="table-dark">
                            <tr>
                                <th scope="col">Name of File</th>
                                <th scope="col">Download</th>                           
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>PRN Residency, Villa Plot Venture, Shadnagar</td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/prnresidency.pdf" download="prnresidency"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Residential Apartments, Mohan Nagar</td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/mohannagaraparment.jpeg" download="mohannagaraparment"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                             <tr>
                                <td>SRD Kakatiya Farms, Warangal </td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/SRDBROUCHER-7 PAGES(1).pdf" download="SRDBROUCHER-7 PAGES(1)"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                             <tr>
                                <td>Srii Bhuvi Developers, Prime Avenue, Chitkul, Patancheru </td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/SRII BHUVI DEVELOPERS BROUCHER(1).pdf" download="SRII BHUVI DEVELOPERS BROUCHER(1)"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Srii Bhuvi Developers, Prime Avenue, Chitkul, Patancheru </td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/SRII BHUVI DEVELOPERS BROUCHER].pdf" download="SRII BHUVI DEVELOPERS BROUCHER"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                             <tr>
                                <td>Mani Farms, Narayankhed, Telangana</td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/manifarms.jpeg" download="manifarms"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                             <tr>
                                <td>Mani Farms, Narayankhed, Telangana</td>
                                <td>
                                   <a class="downloadspan" href="img/downloads/primeavenue.jpeg" download="primeavenue"><i class="fa fa-download" aria-hidden="true"></i>Get it</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--/ table -->
            </div>                       
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>