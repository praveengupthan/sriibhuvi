<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Srii Bhuvi Developers</title>
     <?php include 'includes/styles.php'?>
     <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">About Srii Bhuvi</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">About</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">
            <div class="container">
                <div class="row profile-col py-3 mb-5 justify-content-between rounded">
                    <div class="col-md-3">
                        <img src="img/profile-pic.jpg" alt="" class="img-fluid bhimg">
                    </div>
                    <div class="col-md-9 align-self-center">
                        <article class="pb-1">
                            <h4 class="fbold pb-0 mb-0">Nalini Rao</h4>
                            <p><i>Chief Executive Officer</i></p>
                        </article>
                        <div class="pe-5">
                        <p>Nalini Rao, the visionary CEO of Srii Bhuvi Developers, brings unparalleled expertise and innovation to the real estate industry. With a commitment to excellence, she has spearheaded numerous successful projects, redefining urban landscapes. Her leadership embodies trust, quality, and customer satisfaction, making Srii Bhuvi Developers a trusted name in development.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 align-self-center">
                         <h3 class="h3 fbold uline">We are on expert on this field Better buildings solutions since 2019.</h3>
                         <p>Srii Bhuvi Developers is a truly Indian construction company, founded and built on exceptional people, a commitment to service excellence and a deep-rooted entrepreneurial spirit. We’re living in a world that’s developing fast. Construction, sales, and infrastructure projects and programmes are becoming more complex. The drive for sustainable development is ever more pressing.</p>

                         <p>Srii Bhuvi is a pioneer in India’s Realestate and Construction industry, having executed landmark projects that have defined the country’s progress since 2019. Maintaining our legacy of innovation in engineering and construction, we continue to add new milestones with every passing year, building world-class infrastructure and creating new opportunities for everyone. </p>
                    </div>
                    <div class="col-md-4">
                        <img src="img/about01.jpg" alt="" class="img-fluid bhimg">
                    </div>
                </div>
            </div>

            <!-- why choose us -->
            <div class="whychooseus">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 align-self-center">
                            <img src="img/about02.jpg" alt="" class="img-fluid bhimg">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">Mission Statement</h3>
                                 <ul class="listItems">
                                     <li>A Quality management system</li>
                                     <li>A Quality of Project development</li>
                                     <li>A Quality of Service</li>
                                     <li>A Quality of sound return on investment</li>
                                     <li>Our mission is to generate consistent and sustainable returns for our investors.</li>
                                     <li>To achieve this objective, we continue to practise the values that stand at the core of our professional philosophy</li>
                                     <li>Excellence in investment technique along with the best of personnel in the company allows us to achieve consistent top quality in all our endeavours.</li>
                                     <li>We are dealing property consisting of land and buildings</li>
                                     <li>We strongly believe customer is archer</li>
                                 </ul>
                             </article>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ why choose us -->

            <!-- why choose us -->
            <div class="whychooseus">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 align-self-center order-md-last">
                            <img src="img/about03.jpg" alt="" class="img-fluid bhimg">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">Expertise</h3>
                                 <p>Principal's strong expertise at understanding customer needs in manner of quality construction along with best service in terms of project maintenance after completion of projects, bank loans, flexy mode of payments, why because principal has offered and continues to offer innovative investment options coupled with value added services leads to bright future to customers along with family </p>
                                 <h5> Our Main focus on</h5>
                                 <ul class="listItems">
                                     <li>Developments and services</li>
                                     <li>Renovations</li>
                                     <li>Funding Details</li>
                                     <li>Market and completion</li>
                                     <li>Our Team of experts</li>
                                     <li>Point of Contact</li>
                                     <li>Awards and memberships</li>
                                     <li>Business growth strategy</li>
                                 </ul>
                             </article>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ why choose us -->

            <!-- expert team -->
            <div class="teamSection d-none">
                 <div class="container">
                     <div class="row justify-content-center">
                         <div class="col-md-5 text-center">
                              <h3 class="h3 fbold">Experts team member will ready for your services</h3>
                         </div>
                     </div>
                     <div class="row teamrow">
                         <!-- col -->
                         <div class="col-md-3 col-6">
                             <div class="teamcol">
                                 <img src="img/Worker-2.jpg" alt="" class="img-fluid w-100">
                                 <article class="p-4 text-center">
                                     <h5 class="fsbold m-0 p-0">Nalini Kumari</h5>
                                     <p class="text-center p-0 m-0">
                                         <small>Chief Executive Officer (CEO)</small>
                                     </p>
                                 </article>
                             </div>
                         </div>
                         <!--/ col -->
                          <!-- col -->
                         <div class="col-md-3 col-6">
                             <div class="teamcol">
                                 <img src="img/Worker-1.jpg" alt="" class="img-fluid w-100">
                                 <article class="p-4 text-center">
                                     <h5 class="fsbold m-0 p-0">Employee Name</h5>
                                     <p class="text-center p-0 m-0">
                                         <small>Designation</small>
                                     </p>
                                 </article>
                             </div>
                         </div>
                         <!--/ col -->
                          <!-- col -->
                         <div class="col-md-3 col-6">
                             <div class="teamcol">
                                 <img src="img/Worker-3.jpg" alt="" class="img-fluid w-100">
                                 <article class="p-4 text-center">
                                     <h5 class="fsbold m-0 p-0">Employee Name</h5>
                                     <p class="text-center p-0 m-0">
                                         <small>Designation</small>
                                     </p>
                                 </article>
                             </div>
                         </div>
                         <!--/ col -->
                          <!-- col -->
                         <div class="col-md-3 col-6">
                             <div class="teamcol">
                                 <img src="img/Worker-4.jpg" alt="" class="img-fluid w-100">
                                 <article class="p-4 text-center">
                                     <h5 class="fsbold m-0 p-0">Employee Name</h5>
                                     <p class="text-center p-0 m-0">
                                         <small>Designation</small>
                                     </p>
                                 </article>
                             </div>
                         </div>
                         <!--/ col -->
                     </div>
                 </div>
            </div>
            <!--/ expert team-->

            <!-- build together -->
            <div class="buildtogether">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5 text-center">
                            <h6>SEE OUR WORKS</h6>
                            <h1 class="text-center fbold py-4">Let’s build your future dream together!</h1>
                            <a href="contact.php" class="brdlink">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ build together -->
        </div>
        <!--/ sub page main -->

    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>