<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Srii Bhuvi Developers Blog and News</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Blogs</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blogs</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <?php 
                    for($i=0; $i<count($blogItem);$i++){?>
                    <div class="col-md-4">
                        <div class="blogcol">
                            <figure>
                                <a href="<?php echo $blogItem[$i][0]?>">
                                    <img src="img/blog/<?php echo $blogItem[$i][1]?>" alt="" class="img-fluid w-100">
                                </a>
                            </figure>
                            <article>
                                <p class="date p-0 m-0"><small><?php echo $blogItem[$i][2]?></small></p>
                                <h5 class="fsbold"><a href="blogdetail.php"><?php echo $blogItem[$i][3]?></a></h5>
                                <p><?php echo $blogItem[$i][4]?></p>
                            </article>
                        </div>
                    </div>
                   <?php } ?>
                    <!--/ col -->    
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>