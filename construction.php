<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Building  Construction</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Building  Construction</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item"><a>Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Building  Construction</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">
            <!-- why choose us -->          
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="img/buildingconstruction.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">Buildings & Construction</h3>
                                 <p>Engineering services relating to building design, construction and maintenance have formed a core activity in our operations since the company was founded in 2019.</p>
                                 <p>We offer our clients direct access to a team of highly qualified and experienced specialists offering consultancy services in preliminary work, building design, construction management as well as construction supervision and maintenance work.  Srii Bhuvi has served as engineering consultant on almost every type of building project including, domestic housing, schools, hotels, sports centers, offices, factories, hospitals and health centers.  </p>                                                        
                             </article>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 order-md-last">
                            <img src="img/newbuilding.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">New Buildings</h3>
                                 <h5 class="fsbold fblue">The design of new buildings may be divided into a number of phases.  </h5>
                                 <p>The analysis and design of bearing and support structures are a vital aspect of all construction design and, more often than not, ultimately determine the cost-effectiveness and quality of the final development.  Furthermore, how long a building lasts can depend, to a large extent, on the selection of the materials used in its construction and the arrangement of its support structures, which must be able to resist the external and internal forces to which they will be subjected, both with regard to load-bearing capacity and rigidity.  </p>                                 
                             </article>
                        </div>
                    </div>
                </div>           
            <!--/ why choose us -->
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>