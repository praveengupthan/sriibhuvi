<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Srii Bhuvi Developers Contact</title>
     <?php include 'includes/styles.php'?>
     <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Contact</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row contactDetailsRow">
                    <!-- col -->
                    <div class="col-lg-6 col-xl-3">
                        <div class="contactCol text-center p-4">
                            <span class="icon-address icomoon"></span>
                            <h4 class="py-3 mb-0 fsbold">Address</h4>
                            <p class="text-center">1-1-6, Kotak Mahindra Bank Opp building. 2nd floor, Mohan Nagar, Kothapet, LB Nagar, Hyderabad - 500035.</p>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                    <div class="col-lg-6 col-xl-3">
                        <div class="contactCol text-center p-4">
                            <span class="icon-mail icomoon"></span>
                            <h4 class="py-3 mb-0 fsbold">Mail</h4>
                            <p class="text-center pb-0 mb-0">
                                <a href="mailto:info@sriibhuvidevelopers.com">info@sriibhuvidevelopers.com</a>
                            </p>
                            <p class="text-center">
                                <a href="mailto:info@sriibhuvidevelopers.com">contact@sriibhuvidevelopers.com</a>
                            </p>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                    <div class="col-lg-6 col-xl-3">
                        <div class="contactCol text-center p-4">
                            <span class="icon-phone-call icomoon"></span>
                            <h4 class="py-3 mb-0 fsbold">Phone </h4>
                            <p class="text-center p-0 m-0"><strong>Nalini Rao, CEO</strong>
                                <p class="text-center"><a href="callto:+91 9743947999">+91 9743947999</a></p>
                            </p>
                            <!-- <p class="text-center">
                                <a href="callto:+91 40 4954 1741">+91 40 4954 1741</a>
                            </p> -->
                            <p class="text-center p-0 m-0"><strong>Alivelu Pavan Kumar (MD)</strong>
                                <p class="text-center"><a href="callto:+91 63030 53459">+91 63030 53459</a></p>
                            </p>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 col-xl-3">
                        <div class="contactCol text-center p-4">
                            <span class="icon-clockregular icomoon"></span>
                            <h4 class="py-3 mb-0 fsbold">Hours </h4>
                            <p class="text-center pb-0 mb-0">
                                Mon - Fri: 8AM - 6 PM
                            </p>
                            <p class="text-center pb-0 mb-0">
                                Sat - Sun: 8AM - 3 PM
                            </p>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

            <!-- container fluid -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row py-4">
                    <div class="col-md-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d3807.808414073335!2d78.54593277516454!3d17.37294958351182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMTfCsDIyJzIyLjYiTiA3OMKwMzInNTQuNiJF!5e0!3m2!1sen!2sin!4v1708255496304!5m2!1sen!2sin" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                    <div class="col-md-6">
                        <div class="contactForm">
                             <?php

if (isset($_POST['submitContact'])) {
    $to = "info@sriibhuvidevelopers.com";
    $subject = "Mail From " . $_POST['name'];
    $message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>" . $_POST['name'] . " has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>" . $_POST['name'] . "</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>" . $_POST['phone'] . "</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>" . $_POST['email'] . "</td>
</tr>

<tr>
<th align='left'>Subject</th>
<td>" . $_POST['sub'] . "</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>" . $_POST['msg'] . "</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
    $headers .= 'From:' . $_POST['name'] . "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

    mail($to, $subject, $message, $headers);

//success mesage
    ?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?=$_POST['name']?>, we will contact you shortly.
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>
                            <h4 class="fsbold text-center py-3">Send your Queries</h4>
                            <form id="contact_form" class="form customForm" action="" method="post">
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="nameuser" placeholder="Name" name="name">
                                            <label for="nameuser">Name</label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="phnumber" placeholder="Phone Number" name="phone">
                                            <label for="phnumber">Phone Number</label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="Email" placeholder="Email" name="email">
                                            <label for="Email">Email</label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="Subject" placeholder="Subject" name="sub">
                                            <label for="Subject">Subject</label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-12">
                                        <div class="form-floating mb-3">
                                            <textarea class="form-control" id="msg" style="height:85px;" name="msg"></textarea>
                                            <label for="msg">Message</label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-12">
                                         <button class="btn w-100 text-uppercase brdlink w-100 mt-3" name="submitContact">Submit</button>
                                    </div>
                                    <!--/ col -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!--/ sub page main -->

    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>