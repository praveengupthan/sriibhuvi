<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Layout Development</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Layout Development</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                 <li class="breadcrumb-item"><a>Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Layout Development/li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain"> 
            <!-- why choose us -->          
                <div class="container">
                    <div class="row pt-4">
                        <div class="col-md-6">
                            <img src="img/openlandimg01.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline">Land Development Design Services </h3>
                                 <p>Land development design is an important part of any development project. Right from single-family residential projects to industrial parks, every project requires appropriate land development planning and drafting. </p> 
                                 <ul class="listItems">
                                    <li>Land Development Feasibility Study</li>
                                    <li>Site Planning and Design</li>
                                    <li>Commercial Site Design</li>
                                    <li>Residential/Community Development Design</li>
                                    <li>Environmental Design Services</li>
                                 </ul> 
                             </article>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 order-md-last">
                            <img src="img/layout2.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                             <article>
                                 <h3 class="h3 fbold uline pt-3">We are Offering the Best Real Estate Deals</h3>
                                 <p>At Srii Bhuvi Developers, we have a tendency to believe that once it involves finding a home what is outside the exterior door is simply as necessary as what is behind it. that is why we have a tendency to transcend the everyday listings, by sourcing insights straight from locals and providing over thirty-four neighborhood map overlays, to administer folks a deeper understanding of what living during a home and neighborhood is basically like.  </p>  
                                 <div class="highletSec">
                                     <h6><i>We're committed to serving to them discover an area wherever they're going to like to live and wherever they're going to feel a lot of connections to the community and to every alternative. </i></h6>
                                 </div>                               
                             </article>
                        </div>
                    </div>
                </div>           
            <!--/ why choose us -->
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>