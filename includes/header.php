<!-- header -->
<header class="fixed-top">
    <div class="customContainer">
        <div class="navbar navbar-expand-lg bsnav bg-transparent">
            <a class="navbar-brand" href="index.php">
                <img src="img/logo.svg" alt="">
            </a>
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-center">
                <ul class="navbar-nav navbar-mobile  mx-md-auto">
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?>" href="about.php">About Us</a></li>
                    <li class="nav-item dropdown zoom"><a class="<?php if(
                        basename($_SERVER[ 'SCRIPT_NAME'])=='construction.php' || 
                        basename($_SERVER[ 'SCRIPT_NAME'])=='planningarchitecture.php' || 
                        basename($_SERVER[ 'SCRIPT_NAME'])=='layoutdevelopment.php' ||                        
                        basename($_SERVER[ 'SCRIPT_NAME'])=='interiordesign.php'||
                        basename($_SERVER[ 'SCRIPT_NAME'])=='marketing.php'                       
                        ) {echo 'active'; } else {echo ''; }?> nav-link" href="#">Services<i
                                class="caret"></i></a>
                        <ul class="navbar-nav submenu">
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'construction.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="construction.php">Building Construction</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'planningarchitecture.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="planningarchitecture.php">Planning &amp; Architecture</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'layoutdevelopment.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="layoutdevelopment.php">Layout Development</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'interiordesign.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="interiordesign.php">Interior Design</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'marketing.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="marketing.php">Marketing</a></li>
                        </ul>
                    </li>
                     <li class="nav-item dropdown zoom"><a class="<?php if(
                        basename($_SERVER[ 'SCRIPT_NAME'])=='photogallery.php' || 
                        basename($_SERVER[ 'SCRIPT_NAME'])=='videogallery.php'                      
                       
                        ) {echo 'active'; } else {echo ''; }?> nav-link" href="#">Gallery<i class="caret"></i></a>
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'photogallery.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="photogallery.php">Photo Gallery</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'videogallery.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="videogallery.php">Video Gallery</a></li>                            
                        </ul>
                    </li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='projects.php'){echo'active';}else {echo'nav-link';}?>" href="projects.php">Projects</a></li>
                     <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='downloads.php'){echo'active';}else {echo'nav-link';}?>" href="downloads.php">Downloads</a></li>
                    <li class="nav-item d-none"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='blog.php'){echo'active';}else {echo'nav-link';}?>" href="blog.php">Blog</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='career.php'){echo'active';}else {echo'nav-link';}?>" href="career.php">Careers</a></li>
                </ul>
                <ul class="navbar-nav navbar-mobile ms-md-auto rtnav">
                    <li class="nav-item"><a class="nav-link brdlink" href="contact.php">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>
    </div>
</header>
<!--/ header -->