<!-- styles -->
<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bsnav.min.css">
<link rel="stylesheet" href="css/fonts.css">
<link rel="stylesheet" href="css/swiper.min.css">
<link rel="stylesheet" href="css/baguetteBox.css">
<link rel="stylesheet" href="css/aos.css">
<link rel="stylesheet" href="css/style.css">
<!--/ styles -->