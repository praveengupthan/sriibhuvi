<!-- footer -->
    <footer>

		<a class="whatsappChat" href="https://api.whatsapp.com/send?phone=+919743947999&text=I%27m%20interested%20in%20your%20services" target="_blank">
            <img src="img/WhatsApp_Logo.svg" alt="">
        </a>
        <!-- top footer -->
        <div class="topFooter">
            <div class="customContainer">
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-md-4">
                        <a class="footerBrand" href="javascript:void(0)">
                            <img src="img/logo.svg" alt="" class="footerBrancImg">
                        </a>
                        <article class="pe-md-5">
                            <p class="pt-3">We are on expert on this field Better buildings solutions since 2019. Srii Bhuvi Developers is a truly Indian construction company, founded and built on exceptional people, a commitment to service excellence and a deep-rooted entrepreneurial spirit. We’re living in a world that’s developing fast. <a href="about.php"
                                    class="fblue"><strong>Read More.....</strong></a>
                            </p>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4">
                        <h4 class="pb-3">Contact</h4>
                        <article class="pe-md-5">
                            <table class="table table-borderless">
                                <tr>
                                    <td><span class="icon-address icomoon"></span></td>
                                    <td>
                                        <p>1-1-6, Kotak Mahindra Bank Opp building. 2nd floor, Mohan Nagar, Kothapet, LB Nagar, Hyderabad - 500035.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="icon-mail icomoon"></span></td>
                                    <td>
                                        <p><a href="malto:info@sriibhuvidevelopers.com">info@sriibhuvidevelopers.com</a></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="icon-phone-call icomoon"></span></td>
                                    <td>
                                        <p><a href="tel:9743947999">+91 9743947999</a></p>
                                    </td>
                                </tr>
                            </table>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4">
                        <h4 class="pb-3">Quick links</h4>
                        <div class="d-flex footerlinks">
                            <ul class="pe-md-5">
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'index.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="index.php">Home</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'about.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="about.php">About</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'photogallery.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="photogallery.php">Photo Gallery</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'videogallery.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="videogallery.php">Video Gallery</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'projects.php') {echo 'footeractive';} else {echo 'footerlink';}?> " href="projects.php">Projects</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'career.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="career.php">Careers</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'contact.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="contact.php">Contact</a></li>
                            </ul>
                            <ul>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'construction.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="construction.php">Building Construction</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'planningarchitecture.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="planningarchitecture.php">Planning Architecture</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'layoutdevelopment.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="layoutdevelopment.php">Layout Development</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'interiordesign.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="interiordesign.php">Interior Design</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'marketing.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="marketing.php">Marketing</a></li>
                                <li><a class="<?php if (basename($_SERVER['SCRIPT_NAME']) == 'downloads.php') {echo 'footeractive';} else {echo 'footerlink';}?>" href="downloads.php">Downloads</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
            </div>
        </div>
        <!--/ top footer -->
        <!-- bottom footer -->
        <div class="bottomFooter position-relative">
            <div class="customContainer">
                <div class="row justify-content-between">
                    <div class="col-md-6 align-self-center">
                        <p class="p-0 m-0">
                            <small>© 2021 Copy rights Reserved by Srii Bhuvi Developers</small>
                        </p>
                    </div>
                    <div class="col-md-6 text-sm-end">
                        <div class="footerSocial">
                            <a href="https://www.facebook.com/srii.bhuvi.7" target="_blank">
                                <span class="icon-facebook icomoon"></span>
                            </a>
                            <!-- <a href="javascript:void(0)" target="_blank">
                                <span class="icon-linkedin icomoon"></span>
                            </a>
                            <a href="javascript:void(0)" target="_blank">
                                <span class="icon-twitter icomoon"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:void(0)" class="moveTop" id="movetop"><span class="icon-uparrow icomoon"></span></a>
        </div>
        <!--/ bottom footer -->
    </footer>
    <!--/ footer -->