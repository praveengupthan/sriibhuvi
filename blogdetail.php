<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Srii Bhuvi Developers Blog and News</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/data.php' ?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <h1 class="h1">Blogs</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blogs</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpageMain">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-9">
                        <div class="blogcol">
                            <figure>
                                <a href="javascript:void(0)">
                                    <img src="img/blog/blog-1-850x420.jpg" alt="" class="img-fluid w-100">
                                </a>
                            </figure>
                            <article>
                                <p class="date p-0 m-0"><small>Posted on: 17 October 2021</small></p>
                                <h5 class="fsbold"><a href="javascript:void(0)">How construction managers can encourage employee health.</a></h5>
                                <p>The new functions coming to construction for equipment telematics promise to make life easier for owners and equipment managers. Sure, they’re reducing waste and lowering costs, but the real transformation on the horizon is one where advanced streams of data combine to create entirely new ways of managing machines.</p>

                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="img/blog/blog-gallery-1-1024x683.jpg" alt="" class="img-fluid">
                                    </div>
                                    <div class="col-md-6">
                                        <img src="img/blog/blog-gallery-2-1024x683.jpg" alt="" class="img-fluid">
                                    </div>
                                </div>
                                <div class="highletSec my-2">
                                    <p>The whole difference between construction and creation is exactly this: that a thing constructed can only be loved after it is constructed; but a thing created is loved before it exists.</p>
                                </div>
                                <p>Another speaker, John Meese, senior director of heavy equipment at Waste Management Inc., echoed this, citing a cost-saving of $17,000 for the company when it cut idling time of a single Caterpillar 966 wheel loader.</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora facere labore nam corrupti animi? Eos ratione labore expedita. Suscipit repellat beatae eveniet cum explicabo mollitia?</p>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aut minus pariatur debitis maiores repellat, eos aspernatur corrupti consequatur sunt recusandae necessitatibus ut repudiandae ratione dolorum impedit ea ullam reprehenderit ipsa soluta. Fugiat assumenda cum, nam culpa, ullam, voluptatibus provident sit dolorum aperiam molestias non quasi sapiente impedit explicabo eius iusto!</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- right col -->
                    <div class="col-md-3">
                        <h4>Recent Blogs</h4>
                         <ul class="blogCategories py-3">
                            <li>
                                <a href="javascript:void(0)"
                                    class="d-block border-bottom py-3 position-relative">How construction managers can encourage employee health.</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="d-block border-bottom py-3 position-relative">How to estimate structural steel for construction projects.</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="d-block border-bottom py-3 position-relative">The difference between building construction an estimate.</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                    class="d-block border-bottom py-3 position-relative">Essentials bonds and insurance for construction companies.</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                    class="d-block border-bottom py-3 position-relative">The simple build makes building management easier.</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                    class="d-block border-bottom py-3 position-relative">Managing workforce generation in building construction.</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                    class="d-block border-bottom py-3 position-relative">but a thing created is loved before it exists.</a>
                            </li>
                        </ul>
                    </div>
                    <!--/ right col -->
                    
                    
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
           


           

            
        </div>
        <!--/ sub page main -->
        
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>